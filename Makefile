CC = gcc
CFLAGS = -c -std=c99 -O2 -Wall -Wextra -I$(SRC_DIR)
TEST_CFLAGS = -std=c99 -O2 -I$(SRC_DIR)

SRC_DIR = src/
TEST_DIR = test/
BUILD_DIR = build/
DOCS_DIR = docs/

sources = $(SRC_DIR)utf8_decode.c
headers = $(SRC_DIR)utf8_decode.h
test_executables = $(TEST_DIR)$(BUILD_DIR)character_name

all: libutf8.a $(test_executables) $(DOCS_DIR)

utf8_decode.o: $(sources) $(headers)
	$(CC) $(CFLAGS) $(sources)

libutf8.a: utf8_decode.o
	-mkdir $(BUILD_DIR)
	ar rcs $(BUILD_DIR)$@ utf8_decode.o

$(DOCS_DIR): $(sources) $(headers)
	doxygen Doxyfile


# Build rules for test_executables
$(TEST_DIR)$(BUILD_DIR)character_name: $(TEST_DIR)character_name.c libutf8.a
	-mkdir $(TEST_DIR)$(BUILD_DIR)
	$(CC) $(TEST_CFLAGS) $< -L $(BUILD_DIR) -lutf8 -o$@

.PHONY: clean
clean:
	-rm -f *.o
	-rm -rf $(DOCS_DIR)
	-rm -rf $(TEST_DIR)$(BUILD_DIR)
	-rm -rf $(BUILD_DIR)

