#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "utf8_decode.h"

#define CHARACTER_NOT_FOUND ("Character not in list.")


char *  name_for_codepoint(uint32_t cp, FILE * file)
{
    rewind(file);
    
    uint32_t _cp = 0xFFFFFFFE;
    char buffer[1023+1];
    while(!feof(file) && cp != _cp){
        // Read codepoint
        fscanf(file, "%"SCNx32, &_cp);
        // Read tab
        fgetc(file);
        // Read character name + newline
        fgets(buffer, sizeof(buffer), file);
    }

    char * result;
    if(cp == _cp){
        result = malloc(strlen(buffer)+1);
        strcpy(result, buffer);
        return result;
    }

    else{
        result = malloc(strlen(CHARACTER_NOT_FOUND)+1);
        strcpy(result, CHARACTER_NOT_FOUND);
        return result;
    }

    // Should not reach this point
    return NULL;
}

int main(int argc, char * argv[])
{
    FILE * in = stdin;
    FILE * out = stdout;
    uint8_t ch_data[UTF8_MAX_CHARACTER_LENGTH];
    uint32_t cp;
    unsigned int co;

    if(argc < 2){
        fprintf(stderr, "Usage: character_name path_to_name_list_file");
        return EXIT_FAILURE;
    }
    
    FILE * nl_file = fopen(argv[1], "r");
    if(nl_file == NULL){
        perror("Could not open file");
        return EXIT_FAILURE;
    }


    
    while(1){
        // Read leading octet
        fread(ch_data, 1, 1, in);

        // If a newline was read, jump to the next character
        if(ch_data[0] == 0x0A){
            continue;
        }
        
        // This many octets left to read for the current character
        co = utf8_octets_in_character_starting_with(ch_data[0]) - 1;
        fread(ch_data+1, 1, co, in);

        utf8_parse_character(ch_data, &cp);
        printf("Read character: ");
        char * name = name_for_codepoint(cp, nl_file);
        fwrite(ch_data, 1, co+1, out);
        printf(" (0x%02X) - %s", cp, name);
        free(name);
    }

    fclose(nl_file);
    return 0;
}
