/**
 * @file utf8_decode.h
 * @brief Header file for various utf8 decoding subroutines.
 * 
 * @author George Pîrlea
 * @date 2012-10-13
 * @copyright MIT License
 *
 */

#ifndef UTF8_DECODE_H
#define UTF8_DECODE_H

#include <inttypes.h>
#define UTF8_MAX_CHARACTER_LENGTH (4)

/**
 * @enum Utf8OctetType
 * @brief Defines the ways an octet can be interpreted inside an UTF-8 stream.
 */
enum Utf8OctetType{
    /** Octet that doesn't make sense anywhere in an UTF-8 stream. */
    UTF8_INVALID_OCTET,

    /** Character continuation octet. */
    UTF8_CONTINUATION_OCTET,

    /** Leading octet for a one octet character. */
    UTF8_1_OCTET_CHARACTER_LEADING_OCTET,

    /** Leading octet for a two octet character. */
    UTF8_2_OCTET_CHARACTER_LEADING_OCTET,

    /** Leading octet for a three octet character. */
    UTF8_3_OCTET_CHARACTER_LEADING_OCTET,

    /** Leading octet for a four octet character. */
    UTF8_4_OCTET_CHARACTER_LEADING_OCTET,
};

/**
 * @enum Utf8ErrorType
 * @brief Defines how interpreting an UTF-8 stream or character can fail.
 */
enum Utf8ErrorType{
    /** Valid UTF-8. No error. */
    UTF8_VALID,

    /** There's an error, but we're not sure \a exactly what went wrong. */
    UTF8_INVALID,
    
    /** What was passed as a leading octet doesn't make sense as a one. */
    UTF8_ERROR_INVALID_LEADING_OCTET,
    
    /**
     * Expected a continuation octet, but read an invalid octet.
     * See UTF8_ERROR_UNENDED_CHARACTER for a similar error, but where instead
     * of an invalid octet, a leading octet was read.
     */
    UTF8_ERROR_INVALID_CONTINUATION_OCTET,
    
    /** A leading octet was read, but the previous character hasn't ended. */
    UTF8_ERROR_UNENDED_CHARACTER,
    
    /** UTF-8 dictates characters be encoded in the minimum number of octets
     *  possible. Anything longer, even though it may "make sense", is invalid.
     */
    UTF8_ERROR_OVERLONG_CHARACTER,

    /** Encountered an UTF-16 surrogate, which are not valid in UTF-8. */
    UTF8_ERROR_UTF16_SURROGATE,

    /**
     *  The last two codepoints of every plane plus the range U+FDD0 to U+FDEF
     *  are Unicode noncharacters, codepoints that are guaruanteed by the
     *  Unicode standard never to be allocated.*/
    UTF8_ERROR_UNICODE_NON_CHARACTER,

    /** Valid codepoints must be STRICTLY LESS than U+10FFFF. */
    UTF8_ERROR_OVER_LIMIT,
};

enum Utf8OctetType utf8_octet_type(uint8_t octet);
unsigned int utf8_octets_in_character_starting_with(uint8_t octet);
char * utf8_error_description(enum Utf8ErrorType error);
enum Utf8ErrorType utf8_parse_character(
                                    uint8_t octets[UTF8_MAX_CHARACTER_LENGTH],
                                    uint32_t *codepoint);


#endif
