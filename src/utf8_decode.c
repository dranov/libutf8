/**
 * @file utf8_decode.c
 * @brief Implementation file for various utf8 decoding subroutines.
 * 
 * @author George Pîrlea
 * @date 2012-10-13
 * @copyright MIT License
 * 
 */
 
#include "utf8_decode.h"

/**
 * @brief Given an octet, return its Utf8OctetType.
 *
 * @arg \c octet octet to determine Utf8OctetType for.
 * 
 * @return the Utf8OctetType of the given octet.
 */
enum Utf8OctetType utf8_octet_type(uint8_t octet)
{
    if(octet>>6 == 0x02){return UTF8_CONTINUATION_OCTET;}
    if(octet>>7 == 0x00){return UTF8_1_OCTET_CHARACTER_LEADING_OCTET;}
    if(octet>>5 == 0x06){return UTF8_2_OCTET_CHARACTER_LEADING_OCTET;}
    if(octet>>4 == 0x0E){return UTF8_3_OCTET_CHARACTER_LEADING_OCTET;}
    if(octet>>3 == 0x1E){return UTF8_4_OCTET_CHARACTER_LEADING_OCTET;}
    
    return UTF8_INVALID_OCTET;
}

/**
 * @brief Given a leading octet, return the number of octets in an UTF-8
 *  character starting with that leading octet.
 *
 * @arg \c octet leading octet of an UTF-8 character.
 * 
 * @return number of octets in an UTF-8 character starting with \c octet.
 */
unsigned int utf8_octets_in_character_starting_with(uint8_t octet)
{
    enum Utf8OctetType type = utf8_octet_type(octet);
    switch(type)
    {
        case UTF8_1_OCTET_CHARACTER_LEADING_OCTET: return 1;
        case UTF8_2_OCTET_CHARACTER_LEADING_OCTET: return 2;
        case UTF8_3_OCTET_CHARACTER_LEADING_OCTET: return 3;
        case UTF8_4_OCTET_CHARACTER_LEADING_OCTET: return 4;
        default: return 0;
    }    
}

/**
 * @brief Parse an UTF-8 character, given its leading octet and
 *  the next 3 octets as an array.
 *
 * @arg \c octets[] array with a leading octet at index 0, and then the correct
 *  number of continuation bytes at indexes 1, 2 and 3.
 *
 * @arg \c *codepoint pointer to the memory location in which to place the
 *  codepoint of the given character, if it is valid UTF-8.
 *
 * 
 * @return UTF8_VALID, if no error, else returns the error's type.
 *
 *  If there is no error, then it places the character's codepoint and the
 *   number of octets in the character at the provided addresses.
 *
 */
enum Utf8ErrorType utf8_parse_character(uint8_t octets[UTF8_MAX_CHARACTER_LENGTH],
                                    uint32_t *codepoint)
{
    uint32_t _codepoint = 0;
    unsigned int _character_octets = utf8_octets_in_character_starting_with(octets[0]);

    /* Check if first octet is a character leading octet. */
    if(!(_character_octets >= 1 && _character_octets <= 4)){
        return UTF8_ERROR_INVALID_LEADING_OCTET;
    }

    /* If this is a one octet character, we're done. */
    if(_character_octets == 1){
        _codepoint = octets[0];
    }

    /* It has more than octet, so we have to make sure it has enough
     *  continuation bytes.
     */
    for(unsigned int i = 1; i < _character_octets; i++){
        if(utf8_octet_type(octets[i]) == UTF8_INVALID_OCTET){
            return UTF8_ERROR_INVALID_CONTINUATION_OCTET;
         }
        if(utf8_octets_in_character_starting_with(octets[i]) >= 1 &&
            utf8_octets_in_character_starting_with(octets[i]) <= 4){
                return UTF8_ERROR_UNENDED_CHARACTER;
        }
    }

    /* It has enough continuation bytes, so we can read it as a character.
     *  We still have to make sure it isn't overlong, though.
     */

    /* 11 codepoint bits */
    if(_character_octets == 2){
        _codepoint = (octets[0] - 0xC0) << 6;
        _codepoint += (octets[1] - 0x80);
        
        /* Check for overlong, knowing a one octet UTF-8 character has
         *  7 codepoint bits.
         */
        if(_codepoint < (1 << 7)){
            return UTF8_ERROR_OVERLONG_CHARACTER;
        }
    }

    /* 16 codepoint bits */
    if(_character_octets == 3){
        _codepoint = (octets[0] - 0xE0)  << 12;
        _codepoint += (octets[1] - 0x80) << 6;
        _codepoint += (octets[2] - 0x80);
        
        /* Check for overlong, knowing a 2 octet UTF-8 character has
         *  11 codepoint bits.
         */
        if(_codepoint < (1 << 11)){
            return UTF8_ERROR_OVERLONG_CHARACTER;
        }
    }

    /* 21 codepoint bits. */
    if(_character_octets == 4){
        _codepoint = (octets[0] - 0xF0) << 18;
        _codepoint += (octets[1] - 0x80) << 12;
        _codepoint += (octets[2] - 0x80) << 6;
        _codepoint += (octets[3] - 0x80);

        /* Check for overlong, knowing a 3 octet UTF-8 character has
         *  16 codepoint bits.
         */
        if(_codepoint < (1 << 16)){
            return UTF8_ERROR_OVERLONG_CHARACTER;
        }

        /* For 4 octet characters, we also have to make sure the codepoint is
         *  less than or equal to the limit of U+10FFFF.
         */
         if(_codepoint > 0x10FFFF){
             return UTF8_ERROR_OVER_LIMIT;
         }
    }

    /* Check for Unicode noncharacters and surrogates, anything that would be
     *  invalid UTF-8 that is not covered above.
     */
     
    /* Check for UTF-16 surrogates, which shouldn't appear in UTF-8 */
    if(_codepoint >= 0xD800 && _codepoint <= 0xDFFF){
        return UTF8_ERROR_UTF16_SURROGATE;
    }
    
    /* Check for noncharacter range U+FDD0 to U+FDEF */
    if(_codepoint >= 0xFDD0 && _codepoint <= 0xFDEF){
        return UTF8_ERROR_UNICODE_NON_CHARACTER;
    }

    /* Check for the last two characters in the Unicode planes, which are
     *  noncharacters and should have no place in valid UTF-8
     */
    for(unsigned int plane = 0; plane <= 16; plane++){
        if(_codepoint == (plane << 16) + 0xFFFE ||
            _codepoint == (plane << 16) + 0xFFFF){
                return UTF8_ERROR_UNICODE_NON_CHARACTER;
        }
    }

    /* Everything should be okay at this point. */
    *codepoint = _codepoint;
    return UTF8_VALID;
}

/**
 * @brief Given an error, return a human-readable description of it.
 *
 * @arg \c error - error to return the description to
 * 
 * @return Human-readable description of \c error.
 */
char * utf8_error_description(enum Utf8ErrorType error)
{
    char * desc;
    switch(error)
    {
        case UTF8_VALID: desc = "Valid UTF-8 character."; break;
        case UTF8_INVALID: desc = "Invalid UTF-8 character, but cannot determine reason."; break;
        case UTF8_ERROR_INVALID_CONTINUATION_OCTET: desc = "Invalid continuation octet."; break;
        case UTF8_ERROR_INVALID_LEADING_OCTET: desc = "Invalid leading octet."; break;
        case UTF8_ERROR_UNENDED_CHARACTER: desc = "Unexpected leading octet. Previous character has not ended."; break;
        case UTF8_ERROR_OVERLONG_CHARACTER: desc = "Overlong UTF-8 character."; break;
        case UTF8_ERROR_UTF16_SURROGATE: desc = "UTF-16 surrogate, is invalid UTF-8."; break;
        case UTF8_ERROR_UNICODE_NON_CHARACTER: desc = "Unicode noncharacter."; break;
        case UTF8_ERROR_OVER_LIMIT: desc = "Invalid UTF-8 character, over U+10FFFF limit."; break;
        default: desc = "";
    }
    return desc;
}
 
